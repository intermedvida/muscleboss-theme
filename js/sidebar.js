jQuery( function($) {
	/*
	    Sidebar
	*/

	$('.socio').on('click', function(){
		jivo_api.open();
	});
	
	$('.dismiss, .overlay').on('click', function() {
        $('.sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('.open-menu').on('click', function(e) {
    	e.preventDefault();
        $('.sidebar').addClass('active');
        $('.overlay').addClass('active');
        // close opened sub-menus
        $('.collapse.show').toggleClass('show');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    /* change sidebar style */
	$('a.btn-customized-dark').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').removeClass('light');
	});
	$('a.btn-customized-light').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').addClass('light');
	});
	/* replace the default browser scrollbar in the sidebar, in case the sidebar menu has a height that is bigger than the viewport */
	/*$('.sidebar').mCustomScrollbar({
		theme: "minimal-dark"
	});*/
	
	/*
	    Navigation
	*/
	$('a.scroll-link').on('click', function(e) {
		e.preventDefault();
		scroll_to($(this), 0);
	});

		//Fecha drop down (collapse) minha conta no desktop
	$(document).on('click', function(){
		let navbar = $(".dropdown-toggle");   
		let navul = $('.desk-drop')
			if(navul.hasClass('show')){
				navbar.attr('aria-expanded', false);
				navul.removeClass('show')
			}            
	});

});
