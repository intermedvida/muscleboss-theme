<?php
/**
 * Display all shoppingcart functions and definitions
 *
 * @package Theme Freesia
 * @subpackage ShoppingCart
 * @since ShoppingCart 1.0
 */




/************************************************************************************************/
if ( ! function_exists( 'shoppingcart_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function shoppingcart_setup() {
	/**
	 * Set the content width based on the theme's design and stylesheet.
	 */
	global $content_width;
	if ( ! isset( $content_width ) ) {
			$content_width=1170;
	}

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support('post-thumbnails');
	add_image_size( 'shoppingcart-product-cat-image', 512, 512, true );
	add_image_size( 'shoppingcart-featured-brand-image', 400, 200, true );
	add_image_size( 'shoppingcart-grid-product-image', 420, 420, true );
	add_image_size( 'shoppingcart-popular-post', 75, 75, true );

	/*
	 * Let WordPress manage the document title.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'top-menu' => __( 'Top Menu', 'shoppingcart' ),
		'primary' => __( 'Main Menu', 'shoppingcart' ),
		'catalog-menu' => __( 'Catalog Menu', 'shoppingcart' ),
		'social-link'  => __( 'Add Social Icons Only', 'shoppingcart' ),
	) );

	/* 
	* Enable support for custom logo. 
	*
	*/ 
	add_theme_support( 'custom-logo', array(
		'flex-width' => true, 
		'flex-height' => true,
	) );

	add_theme_support( 'gutenberg', array(
			'colors' => array(
				'#f77426',
			),
		) );
	add_theme_support( 'align-wide' );

	// Add support for responsive embeds.
	add_theme_support( 'responsive-embeds' );

	//Indicate widget sidebars can use selective refresh in the Customizer. 
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * Switch default core markup for comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form', 'comment-list', 'gallery', 'caption',
	) );



	/**
	 * Add support for the Aside Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio', 'chat' ) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'shoppingcart_custom_background_args', array(
		'default-color' => '#ffffff',
		'default-image' => '',
	) ) );

	add_editor_style( array( 'css/editor-style.css') );

	if ( class_exists( 'WooCommerce' ) ) {

		/**
		 * Load WooCommerce compatibility files.
		 */
			
		require get_template_directory() . '/woocommerce/functions.php';

	}


}
endif; // shoppingcart_setup
add_action( 'after_setup_theme', 'shoppingcart_setup' );

/***************************************************************************************/
function shoppingcart_content_width() {
	if ( is_page_template( 'page-templates/gallery-template.php' ) || is_attachment() ) {
		global $content_width;
		$content_width = 1920;
	}
}
add_action( 'template_redirect', 'shoppingcart_content_width' );

/***************************************************************************************/
if(!function_exists('shoppingcart_get_theme_options')):
	function shoppingcart_get_theme_options() {
	    return wp_parse_args(  get_option( 'shoppingcart_theme_options', array() ), shoppingcart_get_option_defaults_values() );
	}
endif;

/***************************************************************************************/
require get_template_directory() . '/inc/customizer/shoppingcart-default-values.php';
require get_template_directory() . '/inc/settings/shoppingcart-slider-functions.php';
require get_template_directory() . '/inc/settings/shoppingcart-functions.php';
require get_template_directory() . '/inc/settings/shoppingcart-common-functions.php';

/************************ ShoppingCart Sidebar  *****************************/
require get_template_directory() . '/inc/widgets/widgets-functions/register-widgets.php';
require get_template_directory() . '/inc/widgets/widgets-functions/popular-posts.php';

if ( class_exists('woocommerce')) {
	require get_template_directory() . '/inc/widgets/widgets-functions/grid-column-widget.php';
}

/************************ ShoppingCart Customizer  *****************************/
require get_template_directory() . '/inc/customizer/functions/sanitize-functions.php';
require get_template_directory() . '/inc/customizer/functions/register-panel.php';

function shoppingcart_customize_register( $wp_customize ) {
if(!class_exists('ShoppingCart_Plus_Features')){
	class ShoppingCart_Customize_upgrade extends WP_Customize_Control {
		public function render_content() { ?>
			<a title="<?php esc_html_e( 'Review Us', 'shoppingcart' ); ?>" href="<?php echo esc_url( 'https://wordpress.org/support/view/theme-reviews/shoppingcart/' ); ?>" target="_blank" id="about_shoppingcart">
			<?php esc_html_e( 'Review Us', 'shoppingcart' ); ?>
			</a><br/>
			<a href="<?php echo esc_url( 'https://themefreesia.com/theme-instruction/shoppingcart/' ); ?>" title="<?php esc_html_e( 'Theme Instructions', 'shoppingcart' ); ?>" target="_blank" id="about_shoppingcart">
			<?php esc_html_e( 'Theme Instructions', 'shoppingcart' ); ?>
			</a><br/>
			<a href="<?php echo esc_url( 'https://tickets.themefreesia.com/' ); ?>" title="<?php esc_html_e( 'Support Tickets', 'shoppingcart' ); ?>" target="_blank" id="about_shoppingcart">
			<?php esc_html_e( 'Support Tickets', 'shoppingcart' ); ?>
			</a><br/>
		<?php
		}
	}
	$wp_customize->add_section('shoppingcart_upgrade_links', array(
		'title'					=> __('Important Links', 'shoppingcart'),
		'priority'				=> 1000,
	));
	$wp_customize->add_setting( 'shoppingcart_upgrade_links', array(
		'default'				=> false,
		'capability'			=> 'edit_theme_options',
		'sanitize_callback'	=> 'wp_filter_nohtml_kses',
	));
	$wp_customize->add_control(
		new ShoppingCart_Customize_upgrade(
		$wp_customize,
		'shoppingcart_upgrade_links',
			array(
				'section'				=> 'shoppingcart_upgrade_links',
				'settings'				=> 'shoppingcart_upgrade_links',
			)
		)
	);
}	
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		
	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector' => '.site-title a',
			'container_inclusive' => false,
			'render_callback' => 'shoppingcart_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector' => '.site-description',
			'container_inclusive' => false,
			'render_callback' => 'shoppingcart_customize_partial_blogdescription',
		) );
	}
	
	require get_template_directory() . '/inc/customizer/functions/design-options.php';
	require get_template_directory() . '/inc/customizer/functions/theme-options.php';
	require get_template_directory() . '/inc/customizer/functions/color-options.php' ;
	require get_template_directory() . '/inc/customizer/functions/featured-content-customizer.php' ;
	if ( class_exists( 'WooCommerce' ) ) {

		require get_template_directory() . '/inc/customizer/functions/frontpage-features.php' ;

	}
}
if(!class_exists('ShoppingCart_Plus_Features')){
	// Add Upgrade to Plus Button.
	require_once( trailingslashit( get_template_directory() ) . 'inc/upgrade-plus/class-customize.php' );

	/************************ TGM Plugin Activatyion  *****************************/
	require get_template_directory() . '/inc/tgm/tgm.php';
}

/** 
* Render the site title for the selective refresh partial. 
* @see shoppingcart_customize_register() 
* @return void 
*/ 
function shoppingcart_customize_partial_blogname() { 
bloginfo( 'name' ); 
} 

/** 
* Render the site tagline for the selective refresh partial. 
* @see shoppingcart_customize_register() 
* @return void 
*/ 
function shoppingcart_customize_partial_blogdescription() { 
bloginfo( 'description' ); 
}
add_action( 'customize_register', 'shoppingcart_customize_register' );
/******************* ShoppingCart Header Display *************************/
function shoppingcart_header_display(){
	$shoppingcart_settings = shoppingcart_get_theme_options();

$header_display = $shoppingcart_settings['shoppingcart_header_display'];

		if ($header_display == 'header_logo' || $header_display == 'show_both') {
			shoppingcart_the_custom_logo();
		}
		if ($header_display == 'header_text' || $header_display == 'show_both') {
			echo '<div id="site-detail">';
				if (is_home() || is_front_page()){ ?>
					<h1 id="site-title"> <?php }else{?> <h2 id="site-title"> <?php } ?>
					<a href="<?php echo esc_url(home_url('/'));?>" title="<?php echo esc_html(get_bloginfo('name', 'display'));?>" rel="home"> <?php bloginfo('name');?> </a>
					<?php if(is_home() || is_front_page()){ ?>
					</h1>  <!-- end .site-title -->
					<?php } else { ?> </h2> <!-- end .site-title --> <?php }

					$site_description = get_bloginfo( 'description', 'display' );
					if ($site_description){?>
						<div id="site-description"> <?php bloginfo('description');?> </div> <!-- end #site-description -->
				<?php }
			echo '</div>'; // end #site-detail
		}

}
add_action('shoppingcart_site_branding','shoppingcart_header_display');




function user_login_icon_display(){
	global $Muscleboss;
	?>

	<div class="user-login">
		<a id="a-socio-header" href="https://muscleboss.com.br/minha-conta/detalhes-socio-muscle/">
			<!-- Coroa e Sócio icone header -->
			<?php if( $Muscleboss->user() && $Muscleboss->user()->is_subscriber() ){
				?>
					<small id="socio-header" style="margin-right: 15px">Sócio Muscle</small>
					<i id="socio-header-crown">
						<svg width="30" height="20" viewBox="0 0 30 27" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M18.3176 24.0633L26.8169 13.6635L18.063 16.8253L18.355 6.45803L10.7942 13.4647L7.45838 4.71313L5.04102 17.9249C10.7775 19.335 16.6263 22.7638 18.3176 24.0633Z" fill="#F8D65E" stroke="#4D4D4D"></path>
						</svg>
					</i>
				<?php
				}
			?>
			
			<i class="fa fa-user"></i>
			
		</a>
		<div class="my-user-wrap">
			<?php if ( !is_user_logged_in() ): ?>
				<div class="my-user">Bem vindo visitante</div>
				<div class="my-user-msg">
					
					<a href="<?php echo get_site_url(); ?>/muscle-admin/?redirect_to=https://muscleboss.com.br">Faça login</a> ou <a href="<?php echo get_site_url(); ?>/registrar/">cadastre-se</a>
				</div>
			
			<?php 
				else: 
					global $current_user;
					$socio = get_user_meta($current_user->ID, "muscleboss_socio", true);
					$socio = $socio == "true" || $socio == 1 ? true : false;
						
										
			?>

				<div class="my-user">Bem vindo <?php echo $current_user->display_name; ?></div>
				<div class="my-user-msg">
					
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Minha conta</a> | <a href="<?php echo wp_logout_url( get_site_url() );?>">Sair</a>
				</div>

				<?php endif; ?>
		</div>
	</div>

	<?php
}


add_action('user_login_icon_display','user_login_icon_display');


if ( ! function_exists( 'shoppingcart_the_custom_logo' ) ) : 
 	/** 
 	 * Displays the optional custom logo. 
 	 * Does nothing if the custom logo is not available. 
 	 */ 
 	function shoppingcart_the_custom_logo() { 
		if ( function_exists( 'the_custom_logo' ) ) { 
			the_custom_logo(); 
		}
 	} 
endif;

require get_template_directory() . '/inc/front-page/front-page-features.php';

/************** YITH_WCWL *************************************/
if ( function_exists( 'YITH_WCWL' ) ) {
	function shoppingcart_update_wishlist_count(){
		wp_send_json( YITH_WCWL()->count_products() );
	}
	add_action( 'wp_ajax_update_wishlist_count', 'shoppingcart_update_wishlist_count' );
	add_action( 'wp_ajax_nopriv_update_wishlist_count', 'shoppingcart_update_wishlist_count' );
}


/************** Add to cart ajax autoload *************************************/
add_filter( 'woocommerce_add_to_cart_fragments', 'shoppingcart_woocommerce_add_to_cart_fragment' );

function shoppingcart_woocommerce_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
			<div class="sx-cart-views">
				<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="wcmenucart-contents">
					<i class="fa fa-shopping-basket"></i>
					<span class="cart-value"><?php echo wp_kses_data ( WC()->cart->get_cart_contents_count() ); ?></span>
				</a>
				<div class="my-cart-wrap">
					<div class="my-cart"><?php esc_html_e('Total', 'shoppingcart'); ?></div>
					<div class="cart-total"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></div>
				</div>
			</div>
	<?php

	$fragments['div.sx-cart-views'] = ob_get_clean();

	return $fragments;
}

function muscleboss_enqueue_scripts() {
	$theme_version = "11.8";

	wp_enqueue_style( 'shoppingcart-style', get_stylesheet_uri(), null,$theme_version); //version
	wp_enqueue_style( 'sidebar', get_template_directory_uri() . '/css/sidebar.css', null, $theme_version );
	wp_enqueue_script( 'sidebar', get_template_directory_uri() . '/js/sidebar.js', ['jquery'], $theme_version );
	//wp_enqueue_style( 'sidebar-scroll', get_template_directory_uri() . '/css/jquery.mCustomScrollbar.min.css', null, $theme_version );
	//wp_enqueue_script( 'sidebar-scroll', get_template_directory_uri() . '/js/jquery.mCustomScrollbar.concat.min.js', ['jquery'], $theme_version );
	wp_enqueue_style( 'muscleboss-theme',  get_template_directory_uri() . '/default.css', null, $theme_version );
}

add_action( 'wp_enqueue_scripts', 'muscleboss_enqueue_scripts', 2 );


function muscleboss_remove_title_page_option( $title, $id ) {
	return "";
}

function muscleboss_product_subtitle(){
	global $product;

	$subtitle = get_post_meta( $product->id, '_subtitle_product', true);
	if( !empty(trim($subtitle)) ){
	echo '<div class="product-subtitle">' . $subtitle . '</div>';
	}
}

add_action( 'woocommerce_shop_loop_item_title', 'muscleboss_product_subtitle' );




function muscleboss_shop_loop_item(){
	global $product;
	$socio_price = get_post_meta( $product->id, '_socio_price', true);
	if( !empty(trim($socio_price)) ){
		echo '<div class="tag-socio-price"><span class="s-title">Para sócio:</span><span class="s-sifrao">R$</span><span class="s-price">' . $socio_price . '</span></div>';
	}

}

add_action( 'woocommerce_after_shop_loop_item_title', 'muscleboss_shop_loop_item' );

add_action( 'wp_footer', function(){
	echo '<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>';
} );


add_action( 'woocommerce_before_shop_loop_item_title', function(){
	echo '<div class="product-title-wrap">';
} );
add_action( 'woocommerce_after_shop_loop_item_title', function(){
	echo '</div><div class="product-button-wrap">';
} ); 
add_action( 'woocommerce_after_shop_loop_item', function(){
	echo "</div>";
} );

function checkout_validation(){
	global $Muscleboss;
	

	/*$response = wp_remote_post("https://www.google.com/recaptcha/api/siteverify", [
		'method' => 'POST',
		'headers'   => [
			'Content-type: application/x-www-form-urlencoded'
		],
		'body' => [
			'secret' => '6LdRu3waAAAAAKTt4z3ZNPYeVlfsve9aoYa_eOZ9',
			'response' => $_POST['data']['recaptcha']
		]
	]);

	if( $response['body']['success'] == false )
		$Muscleboss->sendError("Você não passou na validação do recaptcha");*/


	switch($_POST['data']['step']){
		case 1:
			$Muscleboss->validate($_POST['data'],
			[
				'name' => ['required' => 'O Nome é obrigatório', 'name'],
				'phone' => ['required' => 'O Telefone é obrigatório', 'phone'],

			]);
		break;
		case 2:
			
			$Muscleboss->validate($_POST['data'],
			[
				'email' => ['required' => 'O email é obrigatório', 'email'],
				'cpf' => ['required' => 'O CPF é obrigatório', 'cpf'],
			]);
		break;
		case 3: 
			// $Muscleboss->sendError("Deu erro!");

			$Muscleboss->validate($_POST['data'],
			[
				'country' => ['required' => 'O país é obrigatório', 'country'],
				'cep' => ['required' => 'O CEP é obrigatório', 'cep'],
				'address' => ['required' => 'O Endereço é obrigatório', 'address'],
				'state' => ['required' => 'O Estado é obrigatório', 'state'],
				'city' => ['required' => 'A Cidade é obrigatório', 'city'],
				'neighborhood' => ['required' => 'O Bairro é obrigatório', 'neighborhood'],
				'number' => ['required' => 'O Número é obrigatório', 'phonumberne'],
			]);
		break;
		case 4:
			if($_POST['data']['paymentMethod'] == 'pagarme-credit-card'){
				$Muscleboss->validate($_POST['data'],
				[
					'paymentMethod' => ['required' => 'Selecione o método de pagamento', 'paymentMethod'],
					'card_number' => ['required' => 'O número do cartão obrigatório', 'card_number'],
					//'card_expire' => ['required' => 'A data de validade é obrigatória', 'card_expire'],
					'cvv' => ['required' => 'O código de segurança é obrigatório', 'cvv'],
					'parcelas' => ['required' => 'Selecione o número de parcelas', 'parcelas'],
	
				]);
			}
			$Muscleboss->validate($_POST['data'],
				[
					'shipping_method' => ['required' => 'O método de envio não foi selecionado', 'shipping_method'],
					'country' => ['required' => 'O país é obrigatório', 'country'],
					'cep' => ['required' => 'O CEP é obrigatório', 'cep'],
					'address' => ['required' => 'O Endereço é obrigatório', 'address'],
					'state' => ['required' => 'O Estado é obrigatório', 'state'],
					'city' => ['required' => 'A Cidade é obrigatório', 'city'],
					'neighborhood' => ['required' => 'O Bairro é obrigatório', 'neighborhood'],
					'number' => ['required' => 'O Número é obrigatório', 'phonumberne'],
					'email' => ['required' => 'O email é obrigatório', 'email'],
					'cpf' => ['required' => 'O CPF é obrigatório', 'cpf'],
					'name' => ['required' => 'O Nome é obrigatório', 'name'],
					'phone' => ['required' => 'O Telefone é obrigatório', 'phone'],	
				]);
	}

	header('Content-Type: application/json');
	echo json_encode(['status' => 'OK']);
	wp_die();
}
//Validação formulário checkout
add_action( 'wp_ajax_nopriv_checkout_validation', 'checkout_validation');
add_action( 'wp_ajax_checkout_validation', 'checkout_validation' );


add_filter( 'woocommerce_order_button_html', function(){
	return "";
});

 //filter lastname chekcout
 add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
 function custom_override_checkout_fields( $fields ) {
 	unset($fields['billing']['billing_last_name']);
 	return $fields;
 }

 //filter imagem do produto na pagina de checkout
 add_filter( 'woocommerce_cart_item_name', 'ts_product_image_on_checkout', 10, 3 );
function ts_product_image_on_checkout( $name, $cart_item, $cart_item_key ) {
	
    /* Return if not checkout page */
    if ( ! is_checkout() ) {
        return $name;
    }

    /* Get product object */
    $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
 
    /* Get product thumbnail */
    $thumbnail = $_product->get_image();
 
    /* Add wrapper to image and add some css */
    $image = '<div class="ts-product-image" style="width: 50px; height: 50px; display: inline-block; vertical-align: middle;">'
                . $thumbnail .
            '</div>'; 
 
    /* Prepend image to name and return it */
    return $image . $name;
}

add_filter( 'woocommerce_logout_default_redirect_url', function(){
	return 'https://muscleboss.com.br';
},1);

add_filter ( 'woocommerce_thankyou_order_received_text' , 'wpb_thankyou' , 10 , 2 );

//Banner checkout external product
add_action( 'woocommerce_before_checkout_form', 'url_banner_external_product', 10,1);
function url_banner_external_product() { 
	if(isset($_GET['product_id']) && isset($_GET['ep']) ){
		$url = get_post_meta($_GET['product_id'], "_url_banner", true);
		echo "<div class='banner-external'><img src='{$url}'></div>";
	}
	
}; 
	 

add_action( 'woocommerce_checkout_update_order_review', function($post_data){
	global $Muscleboss;
	parse_str($post_data, $arr_post);
	if( isset($arr_post['quantidade']) ){
		foreach( $arr_post['quantidade'] as $key => $value ){
			if( $value >= 0)
			WC()->cart->set_quantity($key, $value);
		}
		
		//$cart = WC()->cart->get_cart_contents() ;
		//$Muscleboss->log(serialize($cart));
	}
	
});


add_filter('show_admin_bar', function($f){
	return false;
}, 100);


//add menu dropdown minha conta PRA MOBILE
if(is_user_logged_in()){
	add_filter( 'wp_nav_menu_items', 'add_myaccount_menu', 10, 3 );

	function add_myaccount_menu($items, $args){
		global $Muscleboss;
		if($Muscleboss->user()->is_subscriber()){
			$items .= "<li><a href=" . wc_get_page_permalink( 'myaccount' ) . ">Sócio Muscle</a></li>";
		}
		
		$items .= 
			"<li class='drop-menu-mobile'>
				<a href='#myAccount' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle' role='button' aria-controls='myAccount'>
					Minha Conta
				</a>
				<ul class='collapse list-unstyled' id='myAccount'>
					<li>
						<a href=" . wc_get_account_endpoint_url('orders') . ">Pedidos</a>
					</li>
					<li>
						<a href=" . wc_get_account_endpoint_url('edit-address') . ">Endereços</a>
					</li>
					<li>
						<a href=" . wc_get_account_endpoint_url('edit-account') . ">Editar Conta</a>
					</li>
				</ul>
			</li>";
		return $items;
	}
}

// add sair ao menu
add_filter( 'wp_nav_menu_items', 'add_sign_in_up_menu', 10, 3 );
function add_sign_in_up_menu($items, $args){
	if(is_user_logged_in()){
		$items .= "<li class='sair-desktop'><a href=" . wp_logout_url( get_site_url() ) . ">Sair</a></li>";
	}
	return $items;
}

//Add menu "area restrita" para quem está logado e não tem perfil de clietne
if(is_user_logged_in() && !current_user_can('customer')){
	add_filter( 'wp_nav_menu_items', 'add_area_restrita_menu', 10, 3 );

	function add_area_restrita_menu($items, $args){
			$items .= "<li><a href=" . get_site_url() . "/wp-admin" . ">Acesso restrito</a></li>";
		return $items;
	}
}

// function so_27023433_disable_checkout_script(){
//     wp_dequeue_script( 'wc-checkout' );
// }
// add_action( 'wp_enqueue_scripts', 'so_27023433_disable_checkout_script' );

add_filter( 'https_local_ssl_verify', '__return_false' );
add_filter( 'https_ssl_verify', '__return_false');
