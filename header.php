<?php
/**
 * Displays the header content
 *
 * @package Theme Freesia
 * @subpackage ShoppingCart
 * @since ShoppingCart 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
$shoppingcart_settings = shoppingcart_get_theme_options(); ?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="google-site-verification" content="uto0jPbwAN6lpqxTPN9QOMz9Iy3EksNJkTnGSpDNOlU" />
	<!--<meta name="theme-color" content="#D1D1D1">-->
	<meta name="facebook-domain-verification" content="aa0qx0z32egedk2k6w7unadct06a6h" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif;
	wp_head(); ?>
	<!-- Event snippet for PGVIEW - MUSCLE conversion page -->
	<script>
	gtag('event', 'conversion', {'send_to': 'AW-652756220/YYxgCKacyPQBEPyJobcC'});
	</script>
	<script>
		var adminAjaxUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
	</script>
	
</head>
<body <?php body_class(); ?>>
<!------------------------ INICIO SIDEBAR ------------------------>
			<nav class="sidebar light">
				<!-- close sidebar menu -->
				<div class="dismiss">
					<i class="fa fa-arrow-left"></i>
				</div>
				
				<div class="side-header">
					<div class="head">
						<img src="https://muscleboss.com.br/wp-content/uploads/2020/08/muscleboss-logo.png" style="width:80px" class="muscle-logo"></img>
						<?php global $Muscleboss;
						if(!$Muscleboss->user()){
							?>
								<div class="welcome-off">Bem vindo, visitante. <br></div>
							<?php
						}else{
							?>
								<div class="welcome-in">Bem vindo,<br> <?php echo explode(" ", $Muscleboss->user()->full_name)[0] ?> </div>
							<?php
						}
						?>
					</div>
					<?php if(!is_user_logged_in()){ ?>
						
						<div class="head">
							<div class=""><a href="<?php echo get_site_url() ?>/muscle-admin/?redirect_to=<?php echo get_site_url() ?> "><i class="fa fa-sign-in"></i> Entrar</a></div>
							<div class=""><a href="<?php echo get_site_url() ?>/registrar"><i class="fa fa-user-plus"></i>
							Cadastrar</a></div>
						</div>
						
					<?php } ?>
					
					<!-- <h3><a href="index.html">Bootstrap 4 Template with Sidebar Menu</a></h3> -->
				</div>
				<div class="side-body">
					<?php if(!($Muscleboss->user() && $Muscleboss->user()->is_subscriber())){
						?>
						<div class="nao-socio">
							<p><strong>Quer mudar de vida?</strong> Tenha acesso a Nutricionista, Personal Trainer, Psicólogo e suplementos a preço de fábrica! Torne-se um Sócio Muscleboss.</p>
							<a class="saiba-mais" href="http://muscleboss.com.br/socio-muscle">Conheça o Sócio Muscle</a>
						</div>
						<?php
					}else{
						?>
						<div class="socio">
							<div class="user-col">
								<p>Falar com:<br>Ana Franco,<br><strong>Nutricionista</strong><br><i class="fa fa-circle"></i></p>
								<a href="#" onclick="window.$_LHC.eventListener.emitEvent('showWidget');"><img src="https://muscleboss.com.br/wp-content/uploads/2021/03/ana.jpg" alt="" style="width: 80px;"></a>
							</div>
							<div class="user-col">
								<p>Falar com:<br>Wagner Rosa,<br><strong>Personal Trainer</strong><br><i class="fa fa-circle"></i></p>
								
								<a href="#" onclick="window.$_LHC.eventListener.emitEvent('showWidget');"><img src="https://muscleboss.com.br/wp-content/uploads/2021/03/wagner.jpg" alt="" style="width: 80px;"></a>
							</div>
							<div class="user-col">
								<p>Falar com:<br>Allan Phelipp,<br><strong>Psicólogo</strong><br><i class="fa fa-circle"></i></p>
								<a href="#" onclick="window.$_LHC.eventListener.emitEvent('showWidget');"><img src="https://muscleboss.com.br/wp-content/uploads/2021/03/allan.jpg" alt="" style="width: 80px;"></a>
							</div>
							
						</div>
						<?php
					}
					?>

					<?php
						wp_nav_menu( array(
							'menu_id' => 'primary',
							'depth'     => 1,
							'container' => 'ul',
							'menu_class'=> 'list-unstyled menu-elements middle-menu'
						) );
					?>
					
				</div>
				<div class="dark-light-buttons">
					<a class="btn btn-primary btn-customized-4 btn-customized-dark" href="#" role="button">Dark</a>
					<a class="btn btn-primary btn-customized-4 btn-customized-light" href="#" role="button">Light</a>
				</div>
				<!-- <div class="buyWhatsapp">Compre pelo WhatsaApp</div> -->

				<div class="new-nav-logo-socio">
					<?php if($Muscleboss->user() && $Muscleboss->user()->is_subscriber()){ 
							echo "<a href=" . wc_get_page_permalink( 'myaccount' ) . ">";
						}else{
							echo "<a href='https://muscleboss.com.br/socio-muscle'>";
						}
					?>
						<img src="https://muscleboss.com.br/wp-content/uploads/2021/04/socio-menu-logo-6.png" style="max-height: 60px">
					</a>
				</div>
				
			</nav>
			<!-- End sidebar -->
			
			<!-- Dark overlay -->
    		<div class="overlay"></div>
<!------------------------ FIM SIDEBAR ------------------------>



<script>
	jQuery(function($){
		$(window).load(function(){
			$("#site-branding").addClass('animate');
			$("#overlay").animate({
				opacity: 0
			}, 1000, function() {
				$("#overlay").css('display', 'none');
				
  			});
		});
	});
</script>

	<?php 
	if ( function_exists( 'wp_body_open' ) ) {

		wp_body_open();

	} else {

		do_action( 'wp_body_open' );

	 } ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#site-content-contain"><?php esc_html_e('Skip to content','shoppingcart'); ?></a>
<!-- Masthead ============================================= -->

<!-- end #masthead -->

<!-- novo navbar desktop e mobile-->
<div class="new-nav-sidebar" style="display:none">
	
	<a class="logo-wrapper" href="<?php echo get_home_url() ?>"><img src="https://muscleboss.com.br/wp-content/uploads/2020/08/muscleboss-logo.png" style="height:100%; width:100%;" class="muscle-logo"></img></a>
	<?php echo do_shortcode('[wcas-search-form]'); ?>
	<div class="new-nav-sidebar-icons">
		<li class="contact">
			<?php if($Muscleboss->user() && $Muscleboss->user()->is_subscriber()){
				echo "<a href='https://api.whatsapp.com/send?phone=5541996418467&amp;text=Olá Muscleboss, gostaria de tirar uma dúvida'>";
			}else{
				echo "<a href='https://api.whatsapp.com/send?phone=5541995273972&amp;text=Olá Muscleboss, gostaria de tirar uma dúvida'>";
			} ?>

			<i class="fa fa-comments"></i><span class="icon-text-nav" style="font-size:12px;"> Fale<br>Agora</span>
			</a>
		</li>
		<!-- DROPPDOWN MENU DESKTOP MINHA CONTA -->
		<?php if(is_user_logged_in()){ ?>
			<li class='drop-menu-desktop'>
				<a href='#myAccount' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle' role='button' aria-controls='myAccount'>
					<i class="fa fa-user"></i><span class="icon-text-nav" style="font-size:12px;"> Minha<br>Conta</span>
				</a>
				<ul class='collapse list-unstyled desk-drop' id='myAccount'>
					<li>
						<a href="<?php echo wc_get_account_endpoint_url('orders') ?>">Pedidos</a>
					</li>
					<li>
						<a href="<?php echo wc_get_account_endpoint_url('edit-address') ?>">Endereços</a>
					</li>
					<li>
						<a href="<?php echo wc_get_account_endpoint_url('edit-account') ?>">Editar Conta</a>
					</li>
					<li>
						<a href="<?php echo wp_logout_url( get_site_url() )?>">Sair</a>
					</li>

				</ul>
			</li>
		<?php }else{ ?>
			<li class="register">
				<a href="<?php echo get_site_url() ?>/registrar">
					<i class="fa fa-user-plus"></i><span class="icon-text-nav"> Entrar ou<br>Cadastrar</span>
				</a>
			</li>
		<?php } ?>
		<li>
			<a href="#" class="wfc_cart_basket">
				<i class="fa fa-shopping-cart"></i><span class="icon-text-nav" style="font-size:12px;">
				<span class="badge badge-danger cart-qtd"><?php global $woocommerce; echo $woocommerce->cart->cart_contents_count; ?>
				</span> <p class="my-cart">Meu<br>Carrinho</p></span>
			</a>
		</li>
	</div>
	<a class="open-menu">
		<i class="fa fa-bars" aria-hidden="true"></i>
	</a>
</div>
<!-- Menu abaixo da navbar -->
<div class="new-nav-wraper">
	<?php
		wp_nav_menu( array(
			'menu_id' => 'primary',
			'depth'     => 1,
			'container' => 'ul',
			'menu_class'=> 'list-unstyled menu-elements middle-menu'
		) );
	?>
	<span class="new-nav-logo-socio">
		<?php if($Muscleboss->user() && $Muscleboss->user()->is_subscriber()){ 
				echo "<a href=" . wc_get_page_permalink( 'myaccount' ) . ">";
			}else{
				echo "<a href='https://muscleboss.com.br/socio-muscle'>";
			}
		?>
			<img src="https://muscleboss.com.br/wp-content/uploads/2021/04/socio-menu-logo-6.png" style="max-height: 60px !important; max-width: unset !important;">
		</a>
	</span>
</div>
<div class="wrap-top"></div>


<!-- Banner ============================================= -->


<!-- Main Page Start ============================================= -->
<div id="site-content-contain"class="site-content-contain">
	<div id="content" class="site-content">
	<?php
	

	if(is_front_page() && class_exists('woocommerce')){
		if($shoppingcart_settings['shoppingcart_display_featured_brand'] =='below-slider') {
			do_action('shoppingcart_display_front_page_product_brand'); // Display below Slider
		}

		do_action('shoppingcart_display_front_page_product_categories');
		if($shoppingcart_settings['shoppingcart_display_featured_brand'] =='below-product-category') {
			do_action('shoppingcart_display_front_page_product_brand');  // Display below Product Category
		}
	}
