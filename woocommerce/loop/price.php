<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

 global $Muscleboss, $product;
 //Esconder preço de produtos quando for sócio pq o preço comum vira o msm preço de sócio
 if($Muscleboss->user() && $Muscleboss->user()->is_subscriber() && $product->is_type('simple')){
    return null;     
 }

if($price_html = $product->get_price_html()){
    echo "<span class='price'> $price_html </span>";
}
