<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" />
   
</head>
<body>
    <?php
        do_action('woocommerce_before_checkout_form'); 
    ?>
    <div class="checkout">
        <div class="buyOnWhatsapp">
            <a href="https://api.whatsapp.com/send?phone=5541995273972&text=Ol%C3%A1%20Muscleboss!%20Gostaria%20de%20fazer%20um%20pedido"><span class="pay-whatsapp">Deseja fazer esta compra pelo WhatsApp? <span style="text-decoration:underline;">Clique aqui</span></span><p></a>
        </div>
        <div class="form-checkout muscle-form">
            <form method="post" novalidate="novalidate" enctype="multipart/form-data" class="anim checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>">
                <div class="row full-checkout-cards">
                    <div class="col-md-6 mb-3">
                        <div class="card register-card card-form-checkout">
                            <div style="display:none;" class="alert alert-danger alert-dismissible fade show" role="alert"><span class="message"></span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                            <!-- Passo1 email -->
                            <div class="step step-one checkout-step1" id="checkout-step1">
                                <div class="steps-message "><span class="fa fa-check text-success mr-2"></span> Precisamos de algumas informações para finalizar a sua compra.</div>
                                <br>
                                <br>
                                <?php 
                                    global $Muscleboss;
                                    $step = 1;
                                    if($Muscleboss->user()->full_name && $Muscleboss->user()->celphone && $Muscleboss->user()->email && $Muscleboss->user()->cpf){
                                        $step = 3;
                                    }
                                ?>
                                <input type="hidden" class="step-form-checkout" value="<?php echo $step; ?>">
                                <div class="row">
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label class="text-muted form-label" for="billing_first_name">Nome completo</label>
                                            <input class="form-control form-input name" type="text" id="billing_first_name" name="billing_first_name" value="<?php echo isset($Muscleboss->user()->full_name) ? $Muscleboss->user()->full_name : '' ; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label class="text-muted form-label" for="billing_phone">Telefone com DDD</label>
                                            <input class="form-control form-input celphone" type="text" id="billing_phone" name="billing_phone" value="<?php echo isset($Muscleboss->user()->celphone) ? $Muscleboss->user()->celphone : '' ; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fim do passo 1 -->
                            <!-- Passo 2 Dados pessoais-->
                            <div class="step step-two checkout-step2" id="checkout-step2">
                            <br>
                            <div class="row">
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label class="text-muted form-label" for="billing_email">Digite um e-mail válido</label>
                                            <input class="form-control form-input email" type="text" id="billing_email" name="billing_email" value="<?php echo isset($Muscleboss->user()->email) ? $Muscleboss->user()->email : '' ; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label class="text-muted form-label" for="billing_cpf">CPF</label>
                                            <input class="form-control form-input cpf" type="tel" id="billing_cpf" name="billing_cpf" value="<?php echo isset($Muscleboss->user()->cpf) ? $Muscleboss->user()->cpf : '' ; ?>">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- Fim do passo 2 -->
                            <!-- Passo 3 Endereço -->
                            <div class="step step-three checkout-step3" id="checkout-step3">
                                <!-- Selecionar endereço ou adicionar novo endereço -->
                                <?php
                                    if($Muscleboss->user() && $Muscleboss->user()->address){
                                ?>
                                
                                    <div class="steps-message"><span class="fa fa-check text-success mr-2"></span> Você esta indo bem! Agora selecione um endereço para receber os eu produto. </div><br>
                                    <div class="select-shipping">
                                        <div class="form-check shipping-radio mb-3">
                                            <input class="form-check-input shipping-radio-input" checked="true" type="radio" name="shipping-address" value="my-address" id="my-address">
                                            <label class="form-check-label shipping-icons" for="my-address">
                                                <small class="text-muted">
                                                <strong><?php echo $Muscleboss->user()->address . ' - ' . $Muscleboss->user()->address_number ?></strong><br> 
                                                <?php echo $Muscleboss->user()->city ?> - <?php echo $Muscleboss->user()->state ?>, <?php echo $Muscleboss->user()->zip_code ?><br>
                                                </small>
                                            </label>
                                        </div>
                                        <div class="form-check shipping-radio mb-3">
                                            <input class="form-check-input shipping-radio-input" type="radio" name="shipping-address" value="new-address" id="new-address">
                                            <label class="form-check-label shipping-icons" for="new-address">
                                                <small class="text-muted">Adicionar um endereço para esse pedido</small>
                                            </label>
                                        </div>
                                    </div>
                                <?php
                                    }else{
                                        ?>
                                            <div class="steps-message"><span class="fa fa-check text-success mr-2"></span> Você esta indo bem! Agora informe um endereço para receber o seu produto. </div><br>
                                        <?php
                                    }
                                ?>
                                
                                <div class="form-address-shipping">
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <label class="text-muted form-label" for="billing_postcode">CEP</label>
                                                <input class="form-control form-input cep input-shipping" id="billing_postcode" type="text"  name="billing_postcode" value="<?php echo isset($Muscleboss->user()->zip_code) ? $Muscleboss->user()->zip_code : '' ; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <input class="form-control country input-shipping" id="billing_country" type="hidden" name="billing_country" value="BR">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <label class="text-muted form-label" for="billing_address_1">Endereço</label>
                                                <input class="form-control form-input input-shipping" type="text" id="billing_address_1" name="billing_address_1"  value="<?php echo isset($Muscleboss->user()->address) ? $Muscleboss->user()->address : '' ; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <label class="text-muted label-select-state" for="billing_state">Estado</label>
                                                <select class="form-control select input-shipping" id="billing_state" name="billing_state">
                                                    <?php
                                                    
                                                        foreach( $Muscleboss->states() as $key => $value ){
                                                            echo "<option value='{$key}'" . ($key == $Muscleboss->user()->state?' selected':'') . ">{$value}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <label class="text-muted form-label" for="billing_city">Cidade</label>
                                                <input class="form-control form-input city input-shipping" type="text" id="billing_city" name="billing_city"  value="<?php echo isset($Muscleboss->user()->city) ? $Muscleboss->user()->city : '' ; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <label class="text-muted form-label" for="billing_neighborhood">Bairro</label>
                                                <input class="form-control form-input neighborhood input-shipping" type="text" id="billing_neighborhood" name="billing_neighborhood"  value="<?php echo isset($Muscleboss->user()->neighborhood) ? $Muscleboss->user()->neighborhood : '' ; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <label class="text-muted form-label" for="billing_number">Número</label>
                                                <input class="form-control form-input number input-shipping" type="text" id="billing_number" name="billing_number"  value="<?php echo isset($Muscleboss->user()->address_number) ? $Muscleboss->user()->address_number : '' ; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <label class="text-muted form-label" for="billing_complement">Complemento - <span class="font-italic">opcional</span></label>
                                                <input class="form-control form-input complement input-shipping" type="text" id="billing_complement" name="billing_complement"  value="<?php echo isset($Muscleboss->user()->address_complement) ? $Muscleboss->user()->address_complement : '' ; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!-- Passo 4 pagamento-->
                             <div class="step step-four checkout-step4" id="checkout-step4">
                                <div class="steps-message"><span class="fa fa-check text-success mr-2"></span> Ótimo! essa é a ultima etapa. Escolha um método de pagamento. </div>
                                <?php woocommerce_checkout_payment(); ?>
                                <input type="hidden" name="recaptcha_token" id="recaptcha_token" value=""/>
                            </div>
                            <!-- Fim do passo 4 -->
                        </div>
                        <div class="row mt-3 is-desktop">
                            <div class="col-md-12">
                                <div class="checkout-buttons">
                                    <button class="btn-prevent btn btn-danger checkout-button-next step checkout-step1 checkout-step2 checkout-step3">Próximo</button>
                                    <button type="submit" class="btn btn-success button-checkout step checkout-step4" 
                                        name="woocommerce_checkout_place_order" id="place_order" onclick="validateForm()"
                                        data-value="Finalizar compra">Finalizar compra
                                    </button>
                                    <button class="btn-prevent text-muted checkout-button-back step checkout-step2 checkout-step3 checkout-step4">Voltar</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-6">
                        <div class="card register-card checkout-detail-cart mb-3">
                            <div class="row" style="color:#777; font-size: 14px;">
                                <div class="col-md-12 text-center mb-3" style="font-weight: 600;">Confira os detalhes do seu pedido</div>
                                <div class="col-lg-7 order-name"><?php echo $Muscleboss->user()->full_name ? $Muscleboss->user()->full_name : ''; ?></div>
                                <div class="col-lg-5 order-phone"><?php echo $Muscleboss->user()->celphone ? $Muscleboss->user()->celphone : ''; ?></div>
                                <div class="col-lg-7 order-email"><?php echo $Muscleboss->user()->email ? $Muscleboss->user()->email : ''; ?></div>
                                <div class="col-lg-5 order-cpf"><?php echo $Muscleboss->user()->cpf ? $Muscleboss->user()->cpf : ''; ?></div>
                                <div class="col mt-3">
                                    <span class="order-address"><?php echo $Muscleboss->user()->address ? $Muscleboss->user()->address : ''; ?></span>
                                    <span class="order-address_number"><?php echo $Muscleboss->user()->address_number ? $Muscleboss->user()->address_number . ', ' : ''; ?></span>
                                    <span class="order-city"><?php echo $Muscleboss->user()->city ? $Muscleboss->user()->city . '-' : ''; ?></span><!--
                                 --><span class="order-state"><?php echo $Muscleboss->user()->state ? $Muscleboss->user()->state . ', ' : ''; ?></span>
                                    <span class="order-neighborhood"><?php echo $Muscleboss->user()->neighborhood ? $Muscleboss->user()->neighborhood. ', '  : ''; ?></span>
                                    <span class="order-zip_code"><?php echo $Muscleboss->user()->zip_code ? $Muscleboss->user()->zip_code : ''; ?></span>
                                </div>
                            </div>
                            <hr class="mb-0">
                            <?php woocommerce_order_review(); ?>
                        </div>
                    </div>
                    
                </div>
                <div class="row is-mobile">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <div class="checkout-buttons">
                                        <button class="btn-prevent btn btn-danger checkout-button-next step checkout-step1 checkout-step2 checkout-step3">Próximo
                                        </button>
                                        <button type="submit" class="btn btn-success button-checkout step checkout-step4" 
                                            name="woocommerce_checkout_place_order" id="place_order" onclick="validateForm()"
                                            data-value="Finalizar compra">Finalizar compra
                                        </button>
                                        <button class="btn-prevent text-muted checkout-button-back step checkout-step2 checkout-step3 checkout-step4">Voltar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
    
    <script>
        let step = parseInt(<?php echo $step ?>);
        //Preencher detalhes do pedido com os campos do formulário ao digitar
        let name = jQuery('#billing_first_name');
        let cpf = jQuery('#billing_cpf');
        let email = jQuery('#billing_email');
        let phone = jQuery('#billing_phone');
        let zip_code = jQuery('#billing_postcode')
        let address = jQuery('#billing_address_1')
        let state = jQuery('#billing_state')
        let city = jQuery('#billing_city')
        let neighborhood = jQuery('#billing_neighborhood')
        let address_number = jQuery('#billing_number')

        function setOrderData(){
            jQuery('.checkout-detail-cart .order-name').text(name.val());
            jQuery('.checkout-detail-cart .order-cpf').text(cpf.val());
            jQuery('.checkout-detail-cart .order-email').text(email.val());
            jQuery('.checkout-detail-cart .order-phone').text(phone.val());
            jQuery('.checkout-detail-cart .order-zip_code').text(zip_code.val());
            jQuery('.checkout-detail-cart .order-address').text(address.val());
            jQuery('.checkout-detail-cart .order-state').text(state.val());
            jQuery('.checkout-detail-cart .order-city').text(city.val() + '-');
            jQuery('.checkout-detail-cart .order-neighborhood').text(neighborhood.val() + ', ');
            jQuery('.checkout-detail-cart .order-address_number').text(address_number.val() + ', ');
        }

        name.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-name').text(name.val());
        });
        cpf.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-cpf').text(cpf.val());
        });
        email.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-email').text(email.val());
        });
        phone.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-phone').text(phone.val());
        });
        zip_code.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-zip_code').text(zip_code.val());
        });
        address.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-address').text(address.val());
        });
        state.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-state').text(state.val());
        });
        city.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-city').text(city.val() + '-');
        });
        neighborhood.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-neighborhood').text(neighborhood.val() + ', ');
        });
        address_number.on('keyup', function(){
            jQuery('.checkout-detail-cart .order-address_number').text(address_number.val() + ', ');
        });
 
        jQuery('input[name=shipping-address]').on('click', function (e){
            showHideformAddress();
        });

        jQuery('.btn-prevent').on('click', function (e){
            e.preventDefault();
        });
        jQuery('form').on('submit', function (e){
            e.preventDefault();
        });
        //Mostrar formulario de endereço
        function showHideformAddress(){
            let shipping_address = null;
            jQuery("input[name=shipping-address]").each((i, e) => {
                if( jQuery(e).is(':checked') ){
                    shipping_address = jQuery(e).val();
                    console.log(shipping_address);
                    return;
                }
            });
            if(shipping_address == 'my-address'){
                jQuery('.form-address-shipping').hide();
            }else{
                jQuery('.form-address-shipping').show();
            }
        }
        jQuery('.shipping-radio-input:checked').trigger('click');

        //Valida os inputs de cada passo do formulário
        jQuery('.checkout-button-next').on('click', function(e){
            e.preventDefault();
            validateForm();
        });
        jQuery('.checkout-button-back').on('click', function(e){
            e.preventDefault();
            nextStep();
        });

        function validateForm(){
            switch(step){
                case 1:
                    muscleboss.loading.add('body');
                    jQuery.post(adminAjaxUrl, {
                        action: 'checkout_validation',
                        data: {
                            name: jQuery('#billing_first_name').val(),
                            phone: jQuery('#billing_phone').val(),
                            step: step
                        }
                    }).done(function(data){
                        if( data.status == 'OK' ){
                            muscleboss.loading.remove();
                            jQuery('.alert').hide();
                            pass = true;
                            nextStep('next');
                            
                        }else{
                            muscleboss.loading.remove();
                            jQuery(".alert").html(data.message).show();
                            muscleboss.goTop();
                        }
                    });
                break;
                case 2:
                    muscleboss.loading.add('body');
                    jQuery.post(adminAjaxUrl, {
                        action: 'checkout_validation',
                        data: {
                            email: jQuery('#billing_email').val(),
                            cpf: jQuery('#billing_cpf').val(),
                            step: step,
                        }
                    }).done(function(data){
                        if( data.status == 'OK' ){
                            muscleboss.loading.remove();
                            jQuery('.alert').hide();
                            pass = true;
                            nextStep('next');
                            
                        }else{
                            muscleboss.loading.remove();
                            jQuery(".alert").html(data.message).show();
                            muscleboss.goTop();
                        }
                    });
                break;
                case 3:
                    muscleboss.loading.add('body');
                    jQuery.post(adminAjaxUrl, {
                        action: 'checkout_validation',
                        data: {
                            cep: jQuery('#billing_postcode').val(),
                            address: jQuery('#billing_address_1').val(),
                            state: jQuery('#billing_state').val(),
                            city: jQuery('#billing_city').val(),
                            neighborhood: jQuery('#billing_neighborhood').val(),
                            number: jQuery('#billing_number').val(),
                            country: jQuery('#billing_country').val(),
                            step: step,
                        }
                    }).done(function(data){
                        if( data.status == 'OK' ){
                            muscleboss.loading.remove();
                            jQuery('.alert').hide();
                            pass = true;
                            nextStep('next');
                            
                        }else{
                            muscleboss.loading.remove();
                            jQuery(".alert").html(data.message).show();
                            muscleboss.goTop();
                        }
                    });
                break;
                case 4:
                    let method = null;
                    let shipping_method = null;
                    jQuery("input[name=payment_method]").each((i, e) => {
                        if( jQuery(e).is(':checked') ){
                            method = jQuery(e).val();
                            return;
                        }
                    });
                    jQuery(".shipping_method").each((i, e) => {
                        if( jQuery(e).is(':checked') ){
                            shipping_method = jQuery(e).val();
                            console.log('asdf', shipping_method)
                            return;
                        }
                    });
 
                    muscleboss.loading.add('body');
                    jQuery.post(adminAjaxUrl, {
                        action: 'checkout_validation',
                        data: {
                            paymentMethod: method,
                            card_number: jQuery('#pagarme-card-number').val(),
                            card_expire: jQuery('#pagarme-card-expiry').val(),
                            cvv: jQuery('#pagarme-card-cvc').val(),
                            parcelas: jQuery('#pagarme-installments').val(),
                            shipping_method: shipping_method,
                            cep: jQuery('#billing_postcode').val(),
                            address: jQuery('#billing_address_1').val(),
                            state: jQuery('#billing_state').val(),
                            city: jQuery('#billing_city').val(),
                            neighborhood: jQuery('#billing_neighborhood').val(),
                            number: jQuery('#billing_number').val(),
                            country: jQuery('#billing_country').val(),
                            email: jQuery('#billing_email').val(),
                            cpf: jQuery('#billing_cpf').val(),
                            name: jQuery('#billing_first_name').val(),
                            phone: jQuery('#billing_phone').val(),
                            recaptcha: jQuery('#recaptcha_token').val(),
                            step: step,
                        }
                    }).done(function(data){
                        if( data.status == 'OK' ){
                            muscleboss.loading.remove();
                            jQuery('.alert').hide();
                            pass = true;
                            showStep();
                            jQuery(".form-checkout form.checkout").submit();
                            
                        }else{
                            muscleboss.loading.remove();
                            jQuery(".alert").html(data.message).show();
                            muscleboss.goTop();
                        }
                    }).fail((data) => {
                        console.log(data)
                    });
                break;
            }

            return;

        
            let elements = document.querySelectorAll('.checkout-step'+step+' input');
            elements.forEach(function(el){
                if(el.value == ''){
                    el.classList.remove('filled');
                    el.classList.add('not-filled');
                }else{
                    el.classList.remove('not-filled');
                    el.classList.add('filled');
                }
                return true;
            });


        }
        //Exibe cada passo do formulário
        showStep();
        function showStep(to = null){
            let allSteps = document.querySelectorAll('.step');
            let stepActive = document.querySelectorAll('.checkout-step'+step);

            allSteps.forEach(function(item){
                if(to !== 'next' || step == 3 || step == 4){
                    item.style = 'display: none;';
                }
            });
            stepActive.forEach(function(item){
                item.style = 'display:block';
            });

            //exibe detalhes do carrinho no passo 4 só para mobile
            if(step == 1 || step == 2 || step == 3){
                jQuery('.register-card.checkout-detail-cart').removeClass('detail-cart-mobile');
            }else{
                jQuery('.register-card.checkout-detail-cart').addClass('detail-cart-mobile');
                setOrderData();
            }
            muscleboss.goTop();
        }
        
        //avança ou retrocede os passos do formulário
        function nextStep(to){
            if(to == 'next'){
                step = step + 1;        
            }else{

                step = step - 1;
            }
            showStep(to);
        }
 
        document.querySelectorAll('.input-shipping').forEach(function(item){
            item.addEventListener('blur', function(event){
                jQuery(document.body).trigger('update_checkout');
            });
        });
    </script>
</body>
</html>