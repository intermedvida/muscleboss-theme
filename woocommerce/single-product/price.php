<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>
<?php
	global $product;
	do_action('muscleboss_single_product_price');
	$preco = number_format( get_post_meta( $product->id, "_socio_price", true), 2, ",",".");
	echo "<div class='price-socio mt-2' style='margin-top: 10px'><span class='text-price-socio'>Preço para sócio <strong>R$</strong></span><strong>{$preco}</strong><a href='" . get_site_url() . "/socio-muscle/' style='color:white;font-size: 13px;display:block;'>Clique aqui para saber mais</a></div>";
?>